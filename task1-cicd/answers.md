# 1. Знакомоство с SonarQube

## Сделайте скриншот успешного прохождения анализа, приложите к решению ДЗ.

![2](Screenshot_2.png)

# 2. Знакомство с Nexus

## В ответе пришлите файл maven-metadata.xml для этого артефекта.

![4](Screenshot_4.png)

![5](Screenshot_5.png)

# 3. Знакомство с Maven

## В ответе пришлите исправленный файл pom.xml.

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
 
  <groupId>com.netology.app</groupId>
  <artifactId>simple-app</artifactId>
  <version>1.0-SNAPSHOT</version>
   <repositories>
    <repository>
      <id>my-repo</id>
      <name>maven-public</name>
      <url>http://158.160.112.86:8081//repository/maven-public/</url>
    </repository>
  </repositories>
  <dependencies>
    <dependency>
      <groupId>netology</groupId>
      <artifactId>java</artifactId>
      <version>8_282</version>
      <classifier>distrib</classifier>
      <type>tar.gz</type>
    </dependency>
  </dependencies>
</project>
```