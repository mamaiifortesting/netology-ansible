# 08-ansible-02-playbook

## 1. Подготовьте свой inventory-файл prod.yml:

Докер-контейнер на Centos7 (система с минимумом ошибок на исходном плейбуке)

![1](screens/1.png)

## 2-4 Sub-tasks:

- [Шаблоны конфигураций для вектора](https://gitlab.com/mamaiifortesting/netology-ansible/-/tree/main/task2/playbook/templates?ref_type=heads)

- [Финальный плейбук с тасками Вектора](https://gitlab.com/mamaiifortesting/netology-ansible/-/blob/main/task2/playbook/site.yml?ref_type=heads)

## 5. Запустите ansible-lint site.yml и исправьте ошибки, если они есть.

На линте ошибок не так много, т.к. ранее поправил плейбук, в котором были основные ошибки с нехваткой прав по части тасок (был добавлен become_method: su)

![5](screens/5.png)

## 6. Попробуйте запустить playbook на этом окружении с флагом --check.

![6](screens/6.png)

## 7. Запустите playbook на prod.yml окружении с флагом --diff. Убедитесь, что изменения на системе произведены.

![7(1)](screens/7(1).png)

![7(2)](screens/7(2).png)

## 8. Повторно запустите playbook с флагом --diff и убедитесь, что playbook идемпотентен.

![8](screens/8.png)
