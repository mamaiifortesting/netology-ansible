resource "yandex_vpc_network" "develop" {
  name = var.vpc_name
}
resource "yandex_vpc_subnet" "develop" {
  name           = var.vpc_name
  zone           = var.default_zone
  network_id     = yandex_vpc_network.develop.id
  v4_cidr_blocks = var.default_cidr
}


data "yandex_compute_image" "clickhouse" {
  family = var.vm_os_family
}
resource "yandex_compute_instance" "platform1" {
  name        = local.vm_clichouse_name
  platform_id = var.vm_platform_id
  resources {
    cores         = var.vms_resources["low"]["cores"]
    memory        = var.vms_resources["low"]["memory"]
    core_fraction = var.vms_resources["low"]["core_fraction"]
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.clickhouse.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.develop.id
    nat       = true
  }

  metadata = {
    serial-port-enable = var.meta_ssh["pub_key"]["serial-port-enable"]
    ssh-keys           = var.meta_ssh["pub_key"]["ssh-keys"]
  }
}

data "yandex_compute_image" "vector" {
  family = var.vm_os_family
}
resource "yandex_compute_instance" "platform2" {
  name        = local.vm_vector_name
  platform_id = var.vm_platform_id
  resources {
    cores         = var.vms_resources["low"]["cores"]
    memory        = var.vms_resources["low"]["memory"]
    core_fraction = var.vms_resources["low"]["core_fraction"]
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.vector.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.develop.id
    nat       = true
  }

  metadata = {
    serial-port-enable = var.meta_ssh["pub_key"]["serial-port-enable"]
    ssh-keys           = var.meta_ssh["pub_key"]["ssh-keys"]
  }
}

data "yandex_compute_image" "lighthouse" {
  family = var.vm_os_family
}
resource "yandex_compute_instance" "platform3" {
  name        = local.vm_lighthouse_name
  platform_id = var.vm_platform_id
  resources {
    cores         = var.vms_resources["low"]["cores"]
    memory        = var.vms_resources["low"]["memory"]
    core_fraction = var.vms_resources["low"]["core_fraction"]
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.lighthouse.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.develop.id
    nat       = true
  }

  metadata = {
    serial-port-enable = var.meta_ssh["pub_key"]["serial-port-enable"]
    ssh-keys           = var.meta_ssh["pub_key"]["ssh-keys"]
  }
}