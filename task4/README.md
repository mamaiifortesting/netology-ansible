## Ответы:

- Созданы репы под роли [vector-role](https://gitlab.com/mamaiifortesting/vector-role) и [lighthouse-role](https://gitlab.com/mamaiifortesting/lighthouse-role).
- Загрузил и установил роль для клика.
- Созданы новые каталоги под роли вектора и лайтхауса, иницирован процесс создания роли через ansible-galaxy.
- Перенесены хендлеры/таски/вары, заполнены meta.yml и readme по ролям в проектах.
- Новые роли добавлены в requirements.yml для загрузки.
- Изменён основной плейбук для настройки хостов через роли. [playbook](https://gitlab.com/mamaiifortesting/netology-ansible/-/blob/main/task4/playbook/site.yml?ref_type=heads).
![1](Screenshot_3.png)