#!/bin/bash

# Функция для поднятия контейнеров
start_containers() {
  echo "Starting containers..."
  docker run -d --name fedora pycontribs/fedora sleep infinity
  docker run -d --name centos centos sleep infinity
  docker run -d --name ubuntu pycontribs/ubuntu sleep infinity
}

# Функция для запуска Ansible Playbook
run_ansible_playbook() {
  echo "Running Ansible Playbook..."
  ansible-playbook --ask-vault-pass -i inventory/prod.yml site.yml
}

# Функция для остановки контейнеров
stop_containers() {
  echo "Stopping containers..."
  docker stop fedora
  docker stop centos
  docker stop ubuntu
  echo "Deleting containers..."
  docker rm fedora
  docker rm centos
  docker rm ubuntu
}

# Вызываем функции
start_containers
run_ansible_playbook
stop_containers

echo "Script executed successfully."