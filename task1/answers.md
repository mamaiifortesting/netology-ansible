# Main sub-tasks:

## [Ссылка на коммит с изменениями по основному заданию](https://gitlab.com/mamaiifortesting/netology-ansible/-/commit/5ccd2f9214b8a8d72858b7bf00a337dd701081ae)

## 1 sub-task:

![1](images/1.png)

## 2 sub-task:

![2](images/2.png)

## 3 sub-task:

![3](images/3.png)

## 4 sub-task:

![4](images/4.png)

## 5-6 sub-tasks:

![5-6](images/5-6.png)

## 7 sub-task:

![7](images/7.png)

## 8 sub-task:

![8](images/8.png)

## 9 sub-task:

Не особо понял в чём заключалась необходимость "выбора" подходящего и для какого сценария, поэтому просто лист плагинов для коннекта.

![9](images/9.png)

## 10-11 sub-tasks:

![10-11](images/10-11.png)

# Additional sub-tasks:

## [Ссылка на коммит по доп. заданию](https://gitlab.com/mamaiifortesting/netology-ansible/-/commit/ceb7d3c8fe86bddb1df964f6968c6223a3061cc4)

## 1 sub-task:
```
ansible-vault decrypt group_vars/deb/examp.yml
ansible-vault decrypt group_vars/el/examp.yml
```
## 2-3 sub-tasks:
![2-3](images/2-3!.png)

## 4 sub-task:
![4](images/4!.png)

## 5 sub-task:
### [Ссылка на скрипт](https://gitlab.com/mamaiifortesting/netology-ansible/-/blob/main/task1/playbook/script.sh?ref_type=heads)

![5](images/5!.png)
