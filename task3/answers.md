# 1-4 Sub-tasks:

- Изменён инветори файл под реалии с новыми ВМ развёрнутыми на YC:

![inventory](screens/1-3.png)

- Добавлены таски по установке nginx/lighthouse в основной [site.yml](https://gitlab.com/mamaiifortesting/netology-ansible/-/blob/main/task3/playbook/site.yml?ref_type=heads)
- Добавлены новые темплейты под [конфиги nginx'a/lighthous'a](https://gitlab.com/mamaiifortesting/netology-ansible/-/tree/main/task3/playbook/templates?ref_type=heads)
- [Добавлены переменные для репы лайтхауса](https://gitlab.com/mamaiifortesting/netology-ansible/-/blob/main/task3/playbook/group_vars/lighthouse/vars.yml?ref_type=heads) 

# 5. Запустите ansible-lint site.yml и исправьте ошибки, если они есть.

Исправлены ошибки: указал корректный флаг для recurse и убрал лишние строки в конце плейбука.

![5](screens/5.png)

# 6. Попробуйте запустить playbook на этом окружении с флагом --check.

Попробовал, свалился на таске по установке nginx'a из-за отсутствия epel-репы для загрузки.

# 7. Запустите playbook на prod.yml окружении с флагом --diff. Убедитесь, что изменения на системе произведены.

Плейбук запущен в два этапа, т.к. не может создаться база после рестарта сервиса, но при ручной проверке запущенной службы клика на ВМ - она запущена + при повторном выполнении таска успешна. Пробовал добавить таймаут на рестарт (не помогло)

Первая часть запуска с --diff до создания базы.
![7](screens/7.png)

Вторая часть запуска с --diff после создания базы.
![7](screens/7!.png)

# 8. Повторно запустите playbook с флагом --diff и убедитесь, что playbook идемпотентен.

Два change статуса из-за форса в гит команде для таски "Lighthouse | Clone from Git"
Иначе была ругань на Local modifications exist in repository (force=no)
![8](screens/8.png)